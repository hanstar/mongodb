# **mongodb 기본**

### find 

테스트 데이터 입력
``` javascript
let movie_data = [
    {
        "name": "toystory3",
        "nation":"usa",
        "rating": 9.0,
        "actor" : ["james","anna","pet"],
		"release_date" : new Date("2018-01-16")
    },
    {
        "name": "3idiot",
        "nation":"india",
        "rating": 9.2,
        "actor" : ["hassn","mark"],
		"release_date" : new Date("2017-03-30")
    },
    {
        "name": "1987",
        "nation":"korea",
        "rating": 9.3,
        "actor" : ["taeri","jungwo"],
		"release_date" : new Date("2017-05-05")
    },
    {
        "name": "the rock",
        "nation":"usa",
        "rating": 8.8,
        "actor" : ["ian","hoakin"],
		"release_date" : new Date("2017-10-18")
    }
]

db.movies.insert(movie_data)
```

``` javascript
db.movies.find({"nation" : "usa"})

db.movies.find({"nation" : "usa"},{"name":1, "rating":1, "_id": 0})

--"$lt", "$lte", "$gt", "$gte" <,<=,>,>=
db.movies.find({"nation":"korea", "release_date" : {"$lt": new Date("2018-01-01")}}, {"name":1, "actor":1})

db.movies.find({"nation":{"$ne" : "usa"}})

db.movies.find({"nation":{"$in" : ["usa","korea"]})

db.movies.find({"$or" : [{"rating":{"$gt":9}}, {"nation":"india"}]})

db.movies.find({"$or" : [{"rating":{"$gt":9}}, {"nation":{"$in": ["usa"]}}]})


```
db.movies.find({"nation" : "usa"})
/* 1 createdAt:2018. 1. 16. 오후 2:52:01*/
{
	"_id" : ObjectId("5a5d93012986eb32ecc2fa37"),
	"name" : "toystory3",
	"nation" : "usa",
	"rating" : 9,
	"actor" : [
		"james",
		"anna",
		"pet"
	],
	"release_date" : ISODate("2018-01-16T09:00:00.000+09:00")
},

/* 2 createdAt:2018. 1. 16. 오후 2:52:01*/
{
	"_id" : ObjectId("5a5d93012986eb32ecc2fa3a"),
	"name" : "the rock",
	"nation" : "usa",
	"rating" : 8.8,
	"actor" : [
		"ian",
		"hoakin"
	],
	"release_date" : ISODate("2017-10-18T09:00:00.000+09:00")
}


db.movies.find({"nation" : "usa"},{"name":1, "rating":1, "_id": 0})

/* 1 */
{
	"name" : "toystory3",
	"rating" : 9
},

/* 2 */
{
	"name" : "the rock",
	"rating" : 8.8
}



"$lt", "$lte", "$gt", "$gte" <,<=,>,>=

db.movies.find({"nation":"korea", "release_date" : {"$lt": new Date("2018-01-01")}}, {"name":1, "actor":1})

{
	"_id" : ObjectId("5a5d93012986eb32ecc2fa39"),
	"name" : "1987",
	"actor" : [
		"taeri",
		"jungwo"
	]
}


db.movies.find({"nation":{"$ne" : "usa"}})
/* 1 createdAt:2018. 1. 16. 오후 2:52:01*/
{
	"_id" : ObjectId("5a5d93012986eb32ecc2fa38"),
	"name" : "3idiot",
	"nation" : "india",
	"rating" : 9.2,
	"actor" : [
		"hassn",
		"mark"
	],
	"release_date" : ISODate("2017-03-30T09:00:00.000+09:00")
},

/* 2 createdAt:2018. 1. 16. 오후 2:52:01*/
{
	"_id" : ObjectId("5a5d93012986eb32ecc2fa39"),
	"name" : "1987",
	"nation" : "korea",
	"rating" : 9.3,
	"actor" : [
		"taeri",
		"jungwo"
	],
	"release_date" : ISODate("2017-05-05T09:00:00.000+09:00")
}

$in, $nin 하나의 키에 대한 or 연산
$or 다른 조건 절도 포함가능
가장 첫 번째 인자가 많은 문서와 일치할수록 효율적이다.

db.movies.find({"nation":{"$in" : ["usa","korea"]})

db.movies.find({"$or" : [{"rating":{"$gt":9}}, {"nation":"india"}]})

db.movies.find({"$or" : [{"rating":{"$gt":9}}, {"nation":{"$in": ["usa"]}}]})




db.food.insert({"fruit":["apple","banana","peach"]})
db.food.insert({"fruit":["apple","kumquat","orange"]})
db.food.insert({"fruit":["cherry","banana","apple"]})



db.food.find({"fruit": {"$all": ["apple","banana"]})


db.food.find({"fruit": {"$size": 3}})
크기의 범위로 쿼리


화면,차트,설정


logger
agenda
winston













